"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const task_dto_1 = require("./dto/task.dto");
const swagger_1 = require("@nestjs/swagger");
const task_service_1 = require("./task.service");
const passport_1 = require("@nestjs/passport");
let TaskController = class TaskController {
    constructor(taskService) {
        this.taskService = taskService;
    }
    findAll() {
        return __awaiter(this, void 0, void 0, function* () {
            return this.taskService.findAllByUserId();
        });
    }
    findOne(id, res) {
        return __awaiter(this, void 0, void 0, function* () {
            this.taskService.findOne(id, (err, result) => {
                err ? res.status(common_1.HttpStatus.BAD_REQUEST).send({ error: err }) :
                    res.status(common_1.HttpStatus.OK).send(result);
            });
        });
    }
    create(taskDto, res) {
        return __awaiter(this, void 0, void 0, function* () {
            this.taskService.create(taskDto, err => {
                err ? res.status(common_1.HttpStatus.BAD_REQUEST).send({ error: err['message'] }) :
                    res.status(common_1.HttpStatus.OK).send({ message: 'create success' });
            });
        });
    }
    update(res, id, taskDto) {
        return __awaiter(this, void 0, void 0, function* () {
            this.taskService.update(taskDto, id, err => {
                err ? res.status(common_1.HttpStatus.BAD_REQUEST).send({ error: err['message'] }) :
                    res.status(common_1.HttpStatus.OK).send({ message: 'update success' });
            });
        });
    }
    remove(id, res) {
        return __awaiter(this, void 0, void 0, function* () {
            this.taskService.remove(id, err => {
                err ? res.status(common_1.HttpStatus.BAD_REQUEST).send({ err: err['message'] }) :
                    res.status(common_1.HttpStatus.OK).send({ message: 'remove success' });
            });
        });
    }
};
__decorate([
    common_1.Get(),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], TaskController.prototype, "findAllByUserId", null);
__decorate([
    common_1.Get(':id'),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    __param(0, common_1.Param('id')), __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], TaskController.prototype, "findOne", null);
__decorate([
    common_1.Post(),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    __param(0, common_1.Body()), __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [task_dto_1.TaskDto, Object]),
    __metadata("design:returntype", Promise)
], TaskController.prototype, "create", null);
__decorate([
    common_1.Put(':id'),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    __param(0, common_1.Res()), __param(1, common_1.Param('id')), __param(2, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, task_dto_1.TaskDto]),
    __metadata("design:returntype", Promise)
], TaskController.prototype, "update", null);
__decorate([
    common_1.Delete(':id'),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    __param(0, common_1.Param('id')), __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], TaskController.prototype, "remove", null);
TaskController = __decorate([
    swagger_1.ApiUseTags('tasks'),
    common_1.Controller('/task'),
    __metadata("design:paramtypes", [task_service_1.TaskService])
], TaskController);
exports.TaskController = TaskController;
//# sourceMappingURL=task.controller.js.map