"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a;
const mongoose_1 = require("mongoose");
const common_1 = require("@nestjs/common");
const mongoose_2 = require("../../node_modules/@nestjs/mongoose");
let TaskService = class TaskService {
    constructor(taskModel) {
        this.taskModel = taskModel;
    }
    findAll() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.taskModel.find().exec();
        });
    }
    findOne(id, handle) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.taskModel.findById(String(id), (err, result) => {
                if (result === null) {
                    handle('task not found', result);
                }
                else {
                    handle(err, result);
                }
            });
        });
    }
    create(taskDto, handle) {
        return __awaiter(this, void 0, void 0, function* () {
            const createdTask = new this.taskModel(taskDto);
            return yield createdTask.save(err => {
                handle(err);
            });
        });
    }
    update(taskDto, id, handle) {
        return __awaiter(this, void 0, void 0, function* () {
            const TaskUpdate = {
                title: taskDto.title,
                desc: taskDto.desc,
                done: taskDto.done,
            };
            return yield this.taskModel.findByIdAndUpdate(String(id), TaskUpdate, err => {
                handle(err);
            });
        });
    }
    remove(id, handle) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.taskModel.findByIdAndRemove(String(id), err => {
                handle(err);
            });
        });
    }
};
TaskService = __decorate([
    common_1.Injectable(),
    __param(0, mongoose_2.InjectModel('Task')),
    __metadata("design:paramtypes", [typeof (_a = typeof mongoose_1.Model !== "undefined" && mongoose_1.Model) === "function" && _a || Object])
], TaskService);
exports.TaskService = TaskService;
//# sourceMappingURL=task.service.js.map