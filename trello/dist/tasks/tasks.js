"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
let tasksData = [
    {
        id: 1,
        title: 'python',
        desc: 'create simple rest api',
        done: false,
    },
    {
        id: 2,
        title: 'Node.js',
        desc: 'create simple rest api',
        done: false,
    },
    {
        id: 3,
        title: 'Nest.js',
        desc: 'create simple rest api',
        done: false,
    },
];
exports.tasksData = tasksData;
//# sourceMappingURL=tasks.js.map