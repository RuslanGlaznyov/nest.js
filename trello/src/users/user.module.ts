import { Module } from '@nestjs/common';
import { UserSchema } from './schemas/user.schema';
import {MongooseModule} from '@nestjs/mongoose';
import {UserController} from './user.controller';
import {UsersService} from './users.service';
import { AuthService } from '../auth/auth.service';

@Module({
    imports: [MongooseModule.forFeature([{ name: 'User', schema: UserSchema }])],
    controllers: [UserController],
    providers: [UsersService, AuthService],
})

export class UserModule{}