import { Test } from '@nestjs/testing';
import {UserController} from "./user.controller";
import {UsersService} from "./users.service";
import {AuthService} from "../auth/auth.service";

describe('UsersController', () => {
    let userController: UserController;
    let usersService: UsersService;

    beforeEach( async () => {
        const module = await Test.createTestingModule({
            controllers: [UserController],
            providers: [UsersService, AuthService],
            // imports: [
            //     UserModule
            // ]
            // components: [
            //     {
            //         provide: 'UserSchema',
            //         useValue: UserSchema,
            //     }
            // ]

        }).compile();

        usersService = module.get<UsersService>(UsersService);
        userController = module.get<UserController>(UserController);
    });

    describe('test', () => {
       it('should be true', () => {
          expect(true).toBe(true);
       });
    });

    // describe('findAll', () => {
    //     it('should return an array of cats', async () => {
    //         // const result = ['test'];
    //         jest.spyOn(usersService, 'findAll').mockImplementation(() => Object);
    //
    //         expect(await userController.findAll()).toBe(Object);
    //     });
    // });
});
