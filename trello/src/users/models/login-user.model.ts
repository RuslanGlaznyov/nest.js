import { ApiModelProperty } from '@nestjs/swagger';

export class EmailUserModel {
  readonly email: string;
}

export class PasswordUserModel {
  readonly authorization: string;
}