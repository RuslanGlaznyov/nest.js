import { Controller, Get, Post, Body, Param, HttpStatus, Res, Delete, Put, UseGuards, Headers } from '@nestjs/common';
import { UserDto } from './dto/user.dto';
import { UsersService } from './users.service';
import { User } from './interface/user.interface';
import { AuthGuard } from '@nestjs/passport';
import { PasswordUserModel, EmailUserModel } from './models/login-user.model';
import { AuthService } from '../auth/auth.service';
import {ApiBearerAuth, ApiOAuth2Auth, ApiResponse, ApiUseTags} from '@nestjs/swagger';

@Controller('user')
@ApiUseTags('user')
@ApiOAuth2Auth()
// @ApiBearerAuth()
export class UserController {
  constructor(private readonly userService: UsersService,
              private authService: AuthService) {
  }

  // @Post()
  // async create(@Body() userDto: UserDto, @Res() res) {
  //     const answer = (await this.userService.create(userDto));
  //     answer.errors ? res.status(HttpStatus.BAD_REQUEST).json(answer.errors) : res.status(HttpStatus.CREATED).json(answer);
  // }

  @Get('login/:email')
  @ApiResponse({ status: 200, description: 'success'})
  @ApiResponse({ status: 400, description: 'bad request'})
  async login(@Headers() headers: PasswordUserModel, @Res() res, @Param() params: EmailUserModel) {
    const user = {
        firstName: String,
        lastName: String,
        email: String,
    };

    const userData = {
        email: params.email,
        password: headers.authorization,
    }

    this.userService.findByEmailAndPass(userData.email,   userData.password , (err, result) => {
      if (result === null || err) {
          res.status(HttpStatus.BAD_REQUEST).send({error: 'user not found'});
      }else {
        user.firstName = result.firstName;
        user.lastName = result.lastName;
        user.email = result.email;
        this.authService.createToken(user).then( resp => {
            res.status(HttpStatus.ACCEPTED).send(
                {firstName: result.firstName, lastName: result.lastName, token: resp.accessToken, userId: result._id }
                );

        });
      }
    });

  }
  @Get('email/:email')
  async findEmail(@Param('email') email, @Res() res){
      return this.userService.findOneEmail(email, (err, result) =>{
          err ? res.status(HttpStatus.BAD_REQUEST).send({error: err}):
              res.status(HttpStatus.OK).send({result: true})
      });
  }

  @Get()
  @UseGuards(AuthGuard('jwt'))
  @ApiResponse({ status: 200, description: 'success'})
  @ApiResponse({ status: 403, description: 'forbidden'})
  async findAll(): Promise<User[]> {
    return this.userService.findAll();
  }

  @Get(':id')
  @UseGuards(AuthGuard('jwt'))
  @ApiResponse({ status: 200, description: 'success'})
  @ApiResponse({ status: 403, description: 'Forbidden'})
  async findOne(@Param('id') id, @Res() res, @Body() body: UserDto) {
    this.userService.findOne(id, (err, result ) => {
      err ? res.status(HttpStatus.BAD_REQUEST).send({ error: err }) :
      res.status(HttpStatus.OK).send(result);
    });
  }

  @Post()
  @ApiResponse({ status: 201, description: 'create success'})
  @ApiResponse({ status: 400, description: 'bad request'})
  async create(@Body() userDto: UserDto, @Res() res) {
    this.userService.create(userDto, err => {
      err ? res.status(HttpStatus.BAD_REQUEST).send({ error: err['message'] }) :
      res.status(HttpStatus.CREATED).send({ message: 'create success' });
    });
  }

  @Put(':id')
  @UseGuards(AuthGuard('jwt'))
  @ApiResponse({ status: 201, description: 'update success'})
  @ApiResponse({ status: 400, description: 'bad request'})
  async update(@Res() res, @Param('id') id, @Body() userDto: UserDto) {
    this.userService.update(userDto, id, err => {
      err ? res.status(HttpStatus.BAD_REQUEST).send({ error: err['message'] }) :
        res.status(HttpStatus.CREATED).send({ message: 'update success' });
    });
  }

  @Delete(':id')
  @UseGuards(AuthGuard('jwt'))
  @ApiResponse({ status: 401, description: 'unauthorized'})
  @ApiResponse({ status: 200, description: 'delete success'})
  @ApiResponse({ status: 400, description: 'bad request'})
  async remove(@Param('id') id, @Res() res, @Body() userDto: UserDto) {
    this.userService.remove(id, err => {
      err ? res.status(HttpStatus.BAD_REQUEST).send({ error: err['message'] }) :
        res.status(HttpStatus.OK).send({ message: 'remove success' });
    });
  }

}