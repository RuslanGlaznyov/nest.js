import { Model } from 'mongoose';
import { Body, Injectable, Post } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { UserDto } from './dto/user.dto';
import { User } from './interface/user.interface';
// import { AuthService } from '../auth/auth.service';

@Injectable()
export class UsersService {
  constructor(@InjectModel('User') private readonly userModel: Model<User>) {

  }


  async findAll(): Promise<User[]> {
    return await this.userModel.find().exec();
  }
  async  findOneEmail (email: string, handle) {
    return await this.userModel.findOne({email: email}, (err, result) => {
        if (result === null) {
            handle('email not found', result);
        } else {
            handle(null, result);
        }
    });
  }
  async findByEmailAndPass(searchEmail: string, searchPass: string, handle) {
    return await this.userModel.findOne({ email: searchEmail, password: searchPass }, (err, result) => {
      if (result === null) {
        handle('user not found', result);
      } else {
        handle(err, result);
      }
    });
  }

  async findOne(id: number, handle) {
    return await this.userModel.findById(String(id), (err, result) => {
      if (result === null) {
        handle('user not found', result);
      }else{
        handle(err, result);
      }
    });
  }

  async create(userDto: UserDto, handle): Promise<any> {
      this.userModel.init()
      .then(() => {
        this.userModel.create(userDto, err => {
          handle(err);
        });
      }).catch(err => {
      handle(err);
    });
    // console.log(createdUser);
    // this.userModel.on('index', (errUnique) => {
    //   console.log('ok');
    //   createdUser.save(err => {
    //       handle(err);
    //     });
    //   });
    // return createdUser.save(err => {
    //   handle(err);
    // });
  }

  async update(@Body() userDto: UserDto, id: number, handle) {
    const updateObject = {
      firstName: userDto.firstName,
      lastName: userDto.lastName,
      email: userDto.email,
      password: userDto.password,
    };

    return await  this.userModel.findByIdAndUpdate(String(id), updateObject, err => {
      handle(err);
    });
  }

  async remove(id: number, handle) {
    return await this.userModel.findByIdAndRemove(String(id), err => {
      handle(err);
    });
  }

  async authenticate(@Body() userDto: UserDto, handle){
    const user = {
      email: userDto.email,
      password: userDto.password,
    }
    // const checkedUser = await  this.userModel.findOne(user, (err, result) => {
    //   if (result === null){
    //     handle('error', result);
    //   }else {
    //     handle(err, result);
    //   }
    // });
    // this.authService.createToken(checkedUser);
    return await  this.userModel.findOne(user, (err, result) => {
      if (result === null){
        handle('error', result);
      }else {
        handle(err, result);
      }
    });
  }
}