import { ApiModelProperty } from '@nestjs/swagger';

export class UserDto {
    _id: String;
    @ApiModelProperty()
    firstName: string;
    @ApiModelProperty()
    lastName: string;
    @ApiModelProperty()
    email: string;
    @ApiModelProperty()
    password: string;

}
