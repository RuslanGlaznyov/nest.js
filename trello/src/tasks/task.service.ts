import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { Task } from './interface/tasks.interface';
import { InjectModel } from '../../node_modules/@nestjs/mongoose';
import { TaskDto } from './dto/task.dto';

@Injectable()
export class TaskService {
  constructor(@InjectModel('Task') private readonly taskModel: Model<Task>) {
  }

  async findAllByUserId(userId): Promise<Task[]> {
    return await this.taskModel.find({userId: userId}).exec();
  }

  async findOne(id: number, handle) {
    return await this.taskModel.findById(String(id), (err, result) => {
      if (result === null) {
        handle('task not found', result);
      }else{
        handle(err, result);
      }
    });
  }

  async create(taskDto: TaskDto, handle) {
    const createdTask = new this.taskModel(taskDto);
    return await createdTask.save( err => {
      handle(err);
    });
  }

  async update(taskDto: TaskDto, id: number, handle) {
    const TaskUpdate = {
      title: taskDto.title,
      desc: taskDto.desc,
      done: taskDto.done,
    };
    return await this.taskModel.findByIdAndUpdate(String(id), TaskUpdate, err => {
      handle(err);
    });
  }
  async remove(id: number, handle) {
    return await  this.taskModel.findByIdAndRemove(String(id), err => {
      handle(err);
    });
  }
}
