import * as mongoose from 'mongoose';

export const TaskSchema = new mongoose.Schema({
    userId: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'User',
        required: true,
    },
    title: {
        type: String,
        required: true,
    },
    desc: {
        type: String,
    },
    done: {
        type: Boolean,
        required: true,
    },

});
