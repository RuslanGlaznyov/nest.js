import { ApiModelProperty } from '@nestjs/swagger';

export class TaskDto {
  id: number;
  @ApiModelProperty()
  title: string;
  @ApiModelProperty()
  desc: string;
  @ApiModelProperty()
  done: boolean;
  @ApiModelProperty()
  userId: string

}
