import {Controller, Get, Post, Body, Put, Param, Delete, Res, Request, HttpStatus, UseGuards} from '@nestjs/common';
import { TaskDto } from './dto/task.dto';
import {ApiBearerAuth, ApiResponse, ApiUseTags} from '@nestjs/swagger';
import { TaskService } from './task.service';
import {AuthGuard} from "@nestjs/passport";

@ApiUseTags('tasks')
@Controller('/task')
@ApiBearerAuth()
export class TaskController {
  constructor(private readonly taskService: TaskService) {
  }

  @Get(':userId')
  @UseGuards(AuthGuard('jwt'))
  @ApiResponse({ status: 200, description: 'success'})
  @ApiResponse({status: 403, description: 'Forbidden'})
  async findAllByUserId(@Param('userId') userId) {
    return this.taskService.findAllByUserId(userId);
  }

  @Get(':id')
  @UseGuards(AuthGuard('jwt'))
  @ApiResponse({ status: 200, description: 'success'})
  @ApiResponse({status: 403, description: 'Forbidden'})
  async findOne(@Param('id') id, @Res() res){
    this.taskService.findOne(id, (err, result) => {
      err ? res.status(HttpStatus.BAD_REQUEST).send({error: err}):
        res.status(HttpStatus.OK).send(result);
    });
  }

  @Post()
  @UseGuards(AuthGuard('jwt'))
  @ApiResponse({ status: 201, description: 'success'})
  @ApiResponse({status: 403, description: 'Forbidden'})
  async  create(@Body() taskDto: TaskDto, @Res() res){
      this.taskService.create(taskDto, err => {
      err ? res.status(HttpStatus.BAD_REQUEST).send({error: err['message']}):
        res.status(HttpStatus.CREATED).send({ message: 'create success' });
    });
  }

  @Put(':id')
  @UseGuards(AuthGuard('jwt'))
  @ApiResponse({ status: 201, description: 'success'})
  @ApiResponse({status: 403, description: 'Forbidden'})
  async  update(@Res() res, @Param('id') id, @Body() taskDto: TaskDto) {
      this.taskService.update(taskDto, id, err => {
      err ? res.status(HttpStatus.BAD_REQUEST).send({error: err['message']}) :
        res.status(HttpStatus.CREATED).send({ message: 'update success'});
    });
  }

  @Delete(':id')
  @UseGuards(AuthGuard('jwt'))
  @ApiResponse({ status: 200, description: 'success'})
  @ApiResponse({status: 403, description: 'Forbidden'})
  async remove(@Param('id') id, @Res() res) {
    this.taskService.remove(id, err => {
      err ? res.status(HttpStatus.BAD_REQUEST).send({ err: err['message'] }) :
        res.status(HttpStatus.OK).send({ message: 'remove success' });
    });
  }
}