import * as jwt from 'jsonwebtoken';
import { Injectable } from '@nestjs/common';
import { JwtPayload } from './interfaces/jwt.payload';

@Injectable()
export class AuthService {
  async createToken(userData?) {
    const user = { email: userData.email, firstName: userData.firstName, lastName: userData.lastName };
    const expiresIn = 3600;
    const accessToken = jwt.sign(user, 'secretKey', { expiresIn });
    return {
      expiresIn,
      accessToken,
    };

  }

    async validateUser(payload: JwtPayload): Promise<any> {
        // put some validation logic here
        // for example query user by id/email/username
        return {};
    }
}