import { ApiModelProperty } from '@nestjs/swagger';

export interface JwtPayload {
  id: number;
  firstName: string;
  lastName: string;
  email: string;
  password: string;
}