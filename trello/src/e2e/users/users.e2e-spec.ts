import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import {UsersService} from "../../users/users.service";
import {UserController} from "../../users/user.controller";
import {UserModule} from "../../users/user.module";
import { INestApplication } from '@nestjs/common';

describe('User', () => {
    let app: INestApplication;
    let usersService = { findAll: () => ['test'] };

    beforeEach( async () => {
        const module = await Test.createTestingModule({
            imports: [UserModule],
        })
            .overrideProvider(UsersService)
            .useValue(usersService)
            .compile();

        app = module.createNestApplication();
        await app.init();
    });

    it(`/GET email`, () => {
        return request(app.getHttpServer())
            .get('/user/email/iva@gmail.com')
            .expect(200)
            // .expect({
            //     data: catsService.findAll(),
            // });
    });

    afterAll(async () => {
        await app.close();
    });
});