import { Module } from '@nestjs/common';
import { TasksModule } from './tasks/task.module';
import {UserModule} from "./users/user.module";
import {MongooseModule} from "@nestjs/mongoose";
import {AuthModule} from "./auth/auth.module";

@Module({
  imports: [
      AuthModule,
      TasksModule,
      UserModule,
      MongooseModule.forRoot('mongodb://localhost:27017/trello'),
  ],
})
export class AppModule { }
